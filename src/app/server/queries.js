// import pg from "pg"
const pg=require('pg')

const Pool = pg.Pool

const pool = new Pool({
    user: 'mvsnpyon',
    host: '',
    database: 'mvsnpyon',
    password: 'pw',
    port: 5432,
})
//https://api.elephantsql.com/console/a37e2734-4818-41d9-b897-16616fdddf82/stats?
const createUser = (request, response) => {
    const { password, username,name,day,time } = request.params

    pool.query('INSERT INTO clients (password,username,name,day,time) VALUES ($1,$2, $3,$4,$5)', [password, username,name,day,time], (error, results) => {
        if (error) {

            throw error
        }
        response.status(201).send(`User added with ID: ${results.insertId}`)
    })
}

const updateUser =  (request, response) => {
    const { username,day,time } = request.params

    console.log("updated")
    pool.query('UPDATE clients  SET day = $2 ,time = $3 WHERE username =$1 ', [username,day,time], (error, results) => {
        if (error) {

            throw error
        }
        response.status(201).send(`User added with ID: ${results.insertId}`)
    })
}

const register = (request, response)=>{
    console.log("reg")
    const {username, password,name} = request.params
    pool.query('INSERT INTO clients (username, password,name) VALUES ($1,$2,$3)',[username,password,name],(error, res)=>{
            if (error){
                console.log("Error")
            }
            response.send('Added user');
        }

    )
}
const getUsers = (request, response) => {
    pool.query('SELECT * FROM clients ORDER BY username ASCno', (error, results) => {
        if (error) {
            response.status(404).json(error)
            throw error
        }
        console.log("getStatus")
        response.status(200).json(results.rows)
    })
}

const getUsersByDay = (request, response) => {
    const {day} = request.params
    pool.query('SELECT * FROM clients where day = $1',[day], (error, results) => {
        if (error) {
            response.status(404).json(error)
            throw error
        }
        console.log("getStatus")
        response.status(200).json(results.rows)
    })
}
const deleteUser = (request, response) => {

    const {username} = request.params


    pool.query('DELETE FROM clients WHERE username = $1', [username], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).send(`User deleted with username: ${username}`)
    })
}
module.exports={
    getUsersByDay: getUsersByDay,
    getUsers: getUsers,
    createUser: createUser,
    deleteUser:deleteUser,
    register:register,
    updateUser:updateUser,
}
