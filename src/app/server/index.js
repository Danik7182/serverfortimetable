
const q = require('./queries')
const express = require('express')
const app = express()
let cors = require('cors')

app.use(cors())


// app.get('/users', getUsers)
app.put('/updateUser/:username/:day/:time',q.updateUser)
app.post('/createUser/:username/:password/:name/:day/:time', q.createUser)
app.post('/register/:username/:password/:name',q.register)
app.get('/getUsers', q.getUsers)
app.get('/getUsersByDay/:day',q.getUsersByDay)
app.delete('/delete/:username',q.deleteUser)
app.listen(3000, () => {
    console.log(`App running on port 3000.`)
})

